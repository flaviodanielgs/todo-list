import type { NextPage } from "next";
import HomePage from "../App/Screens/Home";

const Home: NextPage = () => {
  return <HomePage />;
};

export default Home;
